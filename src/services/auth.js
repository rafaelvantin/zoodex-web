import $api from "./index";

const login = (email, password) => {
  return new Promise((resolve, reject) => {
    $api
      .post("/auth/authenticate", { email, password })
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};


const recoveryPassword = (email) => {
  return new Promise((resolve, reject) => {
    $api
      .post("/auth/recovery/forgot", { email })
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const resetPassword = (token, password) => {
  return new Promise((resolve, reject) => {
    $api
      .post("/auth/recovery/reset", { token, password })
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const consultAnimal = () => {
  return new Promise((resolve, reject) => {
    $api
      .get("/animals/:id")
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const listAnimals = (id) => {
  return new Promise((resolve, reject) => {
    $api
      .get("/animals/", {
        headers: {
          zoo_id: id
        }
      })
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const cadastraranimal = (formData) => {
  return new Promise((resolve, reject) => {
    $api
      .post("/animals", formData)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const editaranimal = (id, formData ) => {
  return new Promise((resolve, reject) => {
    $api
      .put(`/animals/${id}`,formData)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const deleteanimal = (id) => {
  return new Promise((resolve, reject) => {
    $api
      .delete(`/animals/${id}`)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const cadastrarzoo = (formData) => {
  return new Promise((resolve, reject) => {
    $api
      .post("/zoo", formData)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const consultZoo = (id) => {
  return new Promise((resolve, reject) => {
    $api
      .get(`/zoo/${id}`)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const editarinst = (formData) => {
  return new Promise((resolve, reject) => {
    $api
      .put("/zoo", formData)
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const consultAllZoo = () => {
  return new Promise((resolve, reject) => {
    $api
      .get("/zoo")
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

const deletezoo = () => {
  return new Promise((resolve, reject) => {
    $api
      .delete("/zoo")
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};
const buscaranimal = (id, id_zoo) => {
  return new Promise((resolve, reject) => {
    $api
      .get("/animals/" + id, { headers: { zoo_id: id_zoo }})
      .catch((error) => reject(error))
      .then(({ data }) => resolve(data));
  });
};

export {
  login,
  recoveryPassword,
  resetPassword,
  listAnimals,
  cadastrarzoo,
  cadastraranimal,
  editarinst,
  consultZoo,
  consultAllZoo,
  deletezoo,
  editaranimal,
  consultAnimal,
  buscaranimal,
  deleteanimal,
  };

