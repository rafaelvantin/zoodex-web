//axios é um consumidor de API's que estamos utilizando para mexer com o BD
import axios from "axios";
import $store from "../stores/store";

const api = axios.create({
  //url da API que contém o back do nosso projeto
  baseURL: "https://zoodexapi.herokuapp.com/",
  //withCredentials:true,
});

//pegando o token do usuário antes de cada request
api.interceptors.request.use((config) => {
  if ($store.getters["user/getToken"] != "")
    config.headers.authorization = "Bearer " + $store.getters["user/getToken"];

  return config;
});

export default api;
