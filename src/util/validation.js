//import moment from 'moment';
import * as VeeValidate from 'vee-validate';
import { extend, ValidationObserver, ValidationProvider } from 'vee-validate';
//import { email, length, max, min, required } from 'vee-validate/dist/rules';
//import { required, email } from 'vee-validate/dist/rules';
import Vue from 'vue';


Object.keys(rules).forEach(rule => {
	extend(rule, rules[rule]);
  });
  
  // with typescript
  for (let [rule, validation] of Object.entries(rules)) {
	extend(rule, {
	  ...validation
	});
  }

//setInteractionMode('eager');

/*extend('length', {
	...length,
	message: 'Invalid lenght for {_field_}',
});

extend('dates', {
	validate: value => {
		const formatedDate = moment(value, 'YYYY/MM/DD');
		return formatedDate ? formatedDate.isBetween('1900-01-01', undefined) : '';
	},
	message: 'Invalid date',
});

extend('min', {
	...min,
	message: 'Invalid {_field_}',
});
extend('max', {
	...max,
	message: 'Invalid {_field_}',
});



extend('regex', {
	validate: value => {
		return /^[a-zA-Z0-9áàâãéèêíïóôõöúçñÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ!@#$%^&*()öÖäÄüÜ ]*$/.test(value);
	},
});
extend('terms', {
	validate: value => {
		return value == true;
	},
	message: 'You must agree to the terms',
});
*/
extend('email', {
	...email,
	message: 'Invalid email',
});
extend('required', {
	...required,
	message: '{_field_} is required',
});


Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
Vue.use(VeeValidate, {mode:"eager"})
