export default {
    namespaced: true,

    state: {
        user: {},
        token: "",
    },

    getters: {
        getUser: state => state.user,
        getUserID: state => state.user._id,
        getToken: state => state.token,
    },

    actions: {
        resetUser({ commit }) {
            commit('RESET_USER');
        },
        updateUser({ commit }, payload) {
            commit('SET_USER', payload);
        },
        updateToken({ commit }, payload) {
            commit('SET_TOKEN', payload);
        },
        resetToken({ commit }) {
            commit('RESET_TOKEN');
        },
    },

    mutations: {
        SET_USER(state, user) {
            state.user = user;
        },

        RESET_USER(state) {
            state.user = '';
        },

        SET_TOKEN(state, {token}) {
            state.token = token;
        },

        RESET_TOKEN(state) {
            state.token = "";
        }
    },
}