import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";

//import UIModule from "./modules/ui";
import User from "./modules/user";

Vue.use(Vuex);
const vuex = new VuexPersist({storage:window.localStorage});

const initialState = {
    user: User.state,
};

export default new Vuex.Store({
    modules: {
        //ui: UIModule,
        user: User
    },
    mutations: {
        reset(state) {
            Object.keys(state).forEach(key => {
                Object.assign(state[key], initialState[key]);
            });
        },
    },
    strict: false,
    plugins: [
        vuex.plugin
    ]
});