import $api from "../services/index";
import $store from "../stores/store";


const checkAuth = (to, from, next) => {
  // isAuthNeed = valor de requireAuth atribuído a cada rota do site, no arquivo ../router/index
  const isAuthNeed = to.matched.some((item) => item.meta.requireAuth);

  if (isAuthNeed) {
    $api.get("/auth").then(({ data }) => {
      if (data.zoo) {
        const zooID = $store.getters["user/getUserID"];

        if (zooID != data.zoo) {
          $store.dispatch("user/resetToken");
          $store.dispatch("user/resetUser");
        }
      } else {
        $store.dispatch("user/resetToken");
        $store.dispatch("user/resetUser");
      }
    });

    const token = $store.getters["user/getToken"];

    if (token) return next();

    return next({ path: "/login" });
  }
  return next();
};

export { checkAuth };
