import vue from "vue";
import vuerouter from "vue-router";

import cadastro from "../views/Cadastro.vue";
import login from "../views/Login.vue";
import recuperarSenha from "../views/RecuperarSenha.vue";
import gerenciar from "../views/Gerenciar";
import editarinst from "../views/EditarInst";
import listaanimais from "../views/ListaAnimais";
import cadastroanimal from "../views/CadastroAnimal";
import redefinirSenha from "../views/RedefinirSenha.vue";
import listaZoos from "../views/ListaZoologicos.vue"
import landingpage from "../views/LandingPage";
import editaranimal from "../views/EditarAnimal.vue";
import qrcode from "../views/QRcode.vue";

import { checkAuth } from "./middleware";
//import { editaranimal } from "../services/auth";



vue.use(vuerouter);

const routes = [
  {
    path: "/",
    name: "LandingPage",
    component: landingpage,
    meta: { requireAuth: false },
  },
  {
    path: "/cadastro",
    name: "Cadastro",
    component: cadastro,
    meta: { requireAuth: false },
  },
  {
    path: "/editaranimal/:id",
    name: "EditarAnimal",
    component: editaranimal,
    meta: { requireAuth: true },
  },
  {
    path: "/login",
    name: "Login",
    component: login,
    meta: { requireAuth: false },
  },
  {
    path: "/recuperar-senha",
    name: "RecuperarSenha",
    component: recuperarSenha,
    meta: { requireAuth: false },
  },
  {
    path: "/recovery/:token",
    name: "RedefirSenha",
    component: redefinirSenha,
    meta: { requireAuth: false },
  },
  {
    path: "/gerenciar",
    name: "Gerenciar",
    component: gerenciar,
    meta: { requireAuth: true },
  },
  {
    path: "/qrcode/:id",
    name: "QRCode",
    component: qrcode,
    meta: { requireAuth: true },
  },
  {
    path: "/editarinst",
    name: "EditarInst",
    component: editarinst,
    meta: { requireAuth: true },
  },
  {
    path: "/listaanimais",
    name: "ListaAnimais",
    component: listaanimais,
    meta: { requireAuth: true },
  },
  {
    path: "/cadastroanimal",
    name: "CadastroAnimal",
    component: cadastroanimal,
    meta: { requireAuth: true },
  },
  {
    path: "/listazoologicos",
    name: "ListaZoologicos",
    component: listaZoos,
    meta: { requireAuth: false },
  },

];

const router = new vuerouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach(checkAuth);

export default router;
