import Vue from "vue";
//------------------------------------------------------
//importando os componentes do site
import App from "./App";
import Login from "./views/Login.vue";
import RecuperarSenha from "./views/RecuperarSenha.vue";
import EditarInst from "./views/EditarInst.vue";
import Header from "./components/Header.vue";
import store from "./stores/store";
import LandingPage from "./views/LandingPage.vue";
//------------------------------------------------------
//importando o arquivo rou:class="{'input': true, 'is-danger': errors[0] }"ter para se trabalhar com rotas no site
import router from "./router";
//------------------------------------------------------
//importando o framework Bulma do CSS
import "../node_modules/bulma/css/bulma.css";
//------------------------------------------------------
//importando ícones para o site
import { library } from "@fortawesome/fontawesome-svg-core";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
//
import VueSweetalert2 from 'vue-sweetalert2';
//
//
import { ValidationProvider, ValidationObserver} from 'vee-validate';
//
import { extend } from 'vee-validate';
//
import { email, min, max, required, ext, between } from 'vee-validate/dist/rules';
//
import VueSplide from '@splidejs/vue-splide';
//
Vue.use( VueSplide );


const options = {
  confirmButtonColor: '#008e06',
  cancelButtonColor: '#B22222',
};

library.add(fas);
extend('between', {
   ...between,
   message: 'insira um valor válido (0-200)',
})
extend('required', {
	...required,
	message: '{_field_} é obrigatório',
});

extend('min', {
	...min,
	message: '{_field_} inválido',
});
extend('ext',{
  ...ext,
  message:'Por favor, insira um arquivo válido! (.png, .pnj, .jpeg)'
})
extend('max', {
	...max,
	message: '{_field_} inválido',
});

extend('email', {
	...email,
	message: 'Email inválido',
});

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
Vue.use(VueSweetalert2, options);

Vue.config.productionTip = false;
//------------------------------------------------------
new Vue({
  router,
  store,
  components: { Login, RecuperarSenha, EditarInst, Header, LandingPage },
  render: (h) => h(App),
}).$mount("#app");
